#!/bin/sh

# Display usage
cpack_usage()
{
  cat <<EOF
Usage: $0 [options]
Options: [defaults in brackets after descriptions]
  --help            print this message
  --prefix=dir      directory in which to install
  --include-subdir  include the step12-1.2.1-Linux subdirectory
  --exclude-subdir  exclude the step12-1.2.1-Linux subdirectory
EOF
  exit 1
}

cpack_echo_exit()
{
  echo $1
  exit 1
}

# Display version
cpack_version()
{
  echo "step12 Installer Version: 1.2.1, Copyright (c) Humanity"
}

# Helper function to fix windows paths.
cpack_fix_slashes ()
{
  echo "$1" | sed 's/\\/\//g'
}

interactive=TRUE
cpack_skip_license=FALSE
cpack_include_subdir=""
for a in "$@"; do
  if echo $a | grep "^--prefix=" > /dev/null 2> /dev/null; then
    cpack_prefix_dir=`echo $a | sed "s/^--prefix=//"`
    cpack_prefix_dir=`cpack_fix_slashes "${cpack_prefix_dir}"`
  fi
  if echo $a | grep "^--help" > /dev/null 2> /dev/null; then
    cpack_usage 
  fi
  if echo $a | grep "^--version" > /dev/null 2> /dev/null; then
    cpack_version 
    exit 2
  fi
  if echo $a | grep "^--include-subdir" > /dev/null 2> /dev/null; then
    cpack_include_subdir=TRUE
  fi
  if echo $a | grep "^--exclude-subdir" > /dev/null 2> /dev/null; then
    cpack_include_subdir=FALSE
  fi
  if echo $a | grep "^--skip-license" > /dev/null 2> /dev/null; then
    cpack_skip_license=TRUE
  fi
done

if [ "x${cpack_include_subdir}x" != "xx" -o "x${cpack_skip_license}x" = "xTRUEx" ]
then
  interactive=FALSE
fi

cpack_version
echo "This is a self-extracting archive."
toplevel="`pwd`"
if [ "x${cpack_prefix_dir}x" != "xx" ]
then
  toplevel="${cpack_prefix_dir}"
fi

echo "The archive will be extracted to: ${toplevel}"

if [ "x${interactive}x" = "xTRUEx" ]
then
  echo ""
  echo "If you want to stop extracting, please press <ctrl-C>."

  if [ "x${cpack_skip_license}x" != "xTRUEx" ]
  then
    more << '____cpack__here_doc____'
This is a License

____cpack__here_doc____
    echo
    echo "Do you accept the license? [yN]: "
    read line leftover
    case ${line} in
      y* | Y*)
        cpack_license_accepted=TRUE;;
      *)
        echo "License not accepted. Exiting ..."
        exit 1;;
    esac
  fi

  if [ "x${cpack_include_subdir}x" = "xx" ]
  then
    echo "By default the step12 will be installed in:"
    echo "  \"${toplevel}/step12-1.2.1-Linux\""
    echo "Do you want to include the subdirectory step12-1.2.1-Linux?"
    echo "Saying no will install in: \"${toplevel}\" [Yn]: "
    read line leftover
    cpack_include_subdir=TRUE
    case ${line} in
      n* | N*)
        cpack_include_subdir=FALSE
    esac
  fi
fi

if [ "x${cpack_include_subdir}x" = "xTRUEx" ]
then
  toplevel="${toplevel}/step12-1.2.1-Linux"
  mkdir -p "${toplevel}"
fi
echo
echo "Using target directory: ${toplevel}"
echo "Extracting, please wait..."
echo ""

# take the archive portion of this file and pipe it to tar
# the NUMERIC parameter in this command should be one more
# than the number of lines in this header file
# there are tails which don't understand the "-n" argument, e.g. on SunOS
# OTOH there are tails which complain when not using the "-n" argument (e.g. GNU)
# so at first try to tail some file to see if tail fails if used with "-n"
# if so, don't use "-n"
use_new_tail_syntax="-n"
tail $use_new_tail_syntax +1 "$0" > /dev/null 2> /dev/null || use_new_tail_syntax=""

tail $use_new_tail_syntax +143 "$0" | gunzip | (cd "${toplevel}" && tar xf -) || cpack_echo_exit "Problem unpacking the step12-1.2.1-Linux"

echo "Unpacking finished successfully"

exit 0
#-----------------------------------------------------------
#      Start of TAR.GZ file
#-----------------------------------------------------------;

� �=kZ �Z}l����Ǚ����q
9Ѥ$w��CH��W�R'q���ds�[�[�w�v/q���N`p+T�U��*��H�?���4U��U�Z�A��V�� h䪐��y{�s�1B�j&Z�}�y�͛7_;�2�dn"�������A����a+5K0������ڱ��=���[[I��R;FK^�9pE��਒�]喪g�0駤�����<i�tm\|�[Z"������;:H0|�\*�����F�@�GL�K�@(�|�}���E:I�]M�%U�WZ�x�(ة�l��
x:=�Sl�`��鏯�N		�z���:���賬�y�]���3��Li��!��U�'�p��;�`t𬖢����b���%Ы"�b���e����ᦴ2��vS:՜V2���Ύ掶����>�춝�uy�c����s���uc�����?�����|Ⅷ~�ή[���u�4nԢ�0Z���,տ�~����ϕ�,8�1|���j~�"��E�Vh�:�����.x�YN�Ⱦ��J �U�R��XvI��f$�Ij�D�X|���s��F.��'�����HZ6�kn���L*Kk�Hr<���\B��X��o�֑��$������D,.��R�p4x��a�SɪH�m����F���x6���g��>����h��q��.~1��@�� ����5�	����!���tBUe�֎%��J%"�Kb��S	)��S�ƘQ%�H+wC<$*ǂ:�P2d�@��Gj	��ڍ��n�ZCm8ZX+^}ч�y�� ��U(��|�RC%����������Z�f���q�
���?��~ω�G~�ޠU��w���X����ɂ[��3�ڂ�[�~΂_a�-x���AϜ:��pڦׂ-���Zg�}<l��Ypq�-��@�5� ��<����/}'I�}���8��_�o��*g�P�n�<��i��<���Ly���q��:HFg���۱�+{��?�����T� ��L�3%[j��:�ׯ��!��8SI�nq��X8+N/��7��_�6ܯ�S<3���9I�կ>l�sf�	*4���j]��?q�+Ή'淊�)�Z#�Č��}�ޡ-��$�8���d�V+>�e��:������A�r�]�Lo%�e{A�����h��,�޳����o2��m.�b���(�(��?ೆú��[�½���~ �[�y�b�n�#�Q#k����'��ؼH��@����篆�ٗuC�S��p�?I�u����藣�F���(����yi�g�t8V8+�:���>�N|��և��}ok��_��@�́��^�P\�8}R7�5�&��_��#�'�7*��-5��I6w��J�<L�q9��ds��Vy7�l�M>�A��}(�5t��@�@ߧ��[�Lm�:�{�;B�y��ª�j߬P�8�v���u�,O�&�bq�
����������M�oh]s��o��@κ�����Z)�����Ue�V<^�O�Z���h�������AC��I��z���<���7>���g*��������D�t�W������u�� ȁ|�ߧ������u߽\>�_<oO�����J�����^�l;�K<���0\�x�.X�~1��(~������bV��&��v���=���~���g`#��^̳���8��1����f��ڎ�#�w'k���������ϱ��~�c�QV��[�e>I�|9��F�`�|�]�����\�+�(�L�#�
7��7^�-����Ɩ6�D��l�tؔ��n����0��4��2��6�ю����5��S�v|���x-9���?M�o�k�x����x��sė�y;~%�sį2�	;����x9��qza�����7��ߎ�$��x�k�W�at� �y�V�+�����U?��!���z�n��L.�~�2�����nf'Pc����|�����_O���b�����s�ϟY�*k7�p�O�8�[�o�œ�a�W�v|��g�bg��X��ݮ�)�'�t����y��������z�}�|���y!?d��v�;���:�o�'\�QO���������K>�^�qƯu�{<��H.�Oz訔��^�q����(��k�|�yj߳�������Z�5�8��{�q9��-&�;�ԟ�2�i������n�s���ǼFy��l�t1Z��8�ɜ�j���P�H���!i 6�$RJ�Iڄ���8$RYi,�I����ͩR"?E�ىɴ�ɩPǦ�Vg!��R�D.�8(�-w������OL'��fUs�HK(99�g�"�&9)PTYJf3���'5��S��,��-"���$u�
͝��H�LJJɆ�ÊB�m������bFVM����;�����4w��߷��-��L@�2����K�h�@��&e��z��3�#փy�����U�<uH�����Y��|!�f	Ck���qΞr�/���xYU��]�{J�HyUN],O,�Z�j˙�==JL��1K�Yi�JZ��1��U�v�L2g�c��9;2�����	-1T�tߔ�c��2YME�c�Zb�qc�|h$��S�J���xB'����3��3j��9����.'�T��M�5�$���Ʋ�3��S7�����q���S�g�������%s�?�	%I�Y�%�D��`���}�aS��~g�-�Y����9b�	Q���.���ׄ����k�Ч�}�ᎈ�xo9���u����I�;3���� �0��Qﷷ�N��xB�00>~{�qF}�/!������I�;����v���R��\a�g޿�{=?��a���x��!�g��A�!b����WA��,��8}��!��}��7��Y�j���ߧH���r�Β�s��]�������Gľ~�{��%����^�����\�"7��x�����R������W᩷���h�s{��ߙ�����cK�cy�c�������g���5'd��>�����k	������k�s	�EN�����>�C}�7�L��� ���������k9�)딇�d��l��w~����n-sl�y������.�Y��]b��\.���r�\>��=� .  