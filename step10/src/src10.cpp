#include <iostream>
#include <cmath>
#include "config.h"

using namespace std;

int main()
{
    #ifdef HAS_LOG
    cout<<"has log, log(5)=" << log(5) <<endl;
    #else
    cout<<"don't have log"<<endl;
    #endif
    #ifdef HAS_ALLOC_A
    cout<<"has alloca" <<endl;
    #else
    cout<<"don't have alloca"<<endl;
    #endif
}