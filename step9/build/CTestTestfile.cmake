# CMake generated Testfile for 
# Source directory: D:/Documents/cmake/step9
# Build directory: D:/Documents/cmake/step9/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(test1 "step9" "hello")
set_tests_properties(test1 PROPERTIES  PASS_REGULAR_EXPRESSION "hello passed")
add_test(test2 "step9")
set_tests_properties(test2 PROPERTIES  PASS_REGULAR_EXPRESSION "No params")
add_test(test3 "step9" "hello world")
set_tests_properties(test3 PROPERTIES  PASS_REGULAR_EXPRESSION "hello world passed")
