#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char ** argv)
{
    if(argc != 2)
    {
        cout<<"params count error"<<endl;
        return -1;
    }
    else
    {
        ofstream of(argv[1]);
        of<<"#define MY_DEFINE"<<endl;
        of.close();
        return 0;
    }
}