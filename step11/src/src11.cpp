#include <iostream>
#include "my.h"
#include "my2.h"

using namespace std;

int main()
{
    cout<<"hello world"<<endl;
    #ifdef MY_DEFINE
    cout<<"MY_DEFINE defined"<<endl;
    #endif
    #ifdef MY_DEFINE2
    cout<<"MY_DEFINE2 defined"<<endl;
    #endif
    return 0;
}