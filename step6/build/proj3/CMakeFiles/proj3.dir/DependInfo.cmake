# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/Documents/cmake/step6/proj3/src/app.cpp" "D:/Documents/cmake/step6/build/proj3/CMakeFiles/proj3.dir/src/app.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../proj3/include"
  "../proj3/../proj1/include"
  "../proj3/../proj2/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "D:/Documents/cmake/step6/build/proj1/CMakeFiles/proj1.dir/DependInfo.cmake"
  "D:/Documents/cmake/step6/build/proj2/CMakeFiles/proj2.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
